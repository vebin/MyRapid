/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
namespace MyRapid.Client
{
    partial class MyHelp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyHelp));
            this.helpEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.SuspendLayout();
            // 
            // helpEdit
            // 
            this.helpEdit.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.helpEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpEdit.Location = new System.Drawing.Point(0, 0);
            this.helpEdit.Name = "helpEdit";
            this.helpEdit.ReadOnly = true;
            this.helpEdit.Size = new System.Drawing.Size(639, 528);
            this.helpEdit.TabIndex = 0;
            // 
            // MyHelp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 528);
            this.Controls.Add(this.helpEdit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MyHelp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }
        #endregion
        private DevExpress.XtraRichEdit.RichEditControl helpEdit;
    }
}