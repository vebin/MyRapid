/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraTab;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MyRapid.Base.Edit
{
    /// <summary>
    /// 支持Form.ValidateChildren的XtraTabControl
    /// </summary>
    public class TabEx : XtraTabControl
    {
        //this.SetStyle(System.Windows.Forms.ControlStyles.ContainerControl, true);
        public TabEx()
        {
            this.SetStyle(System.Windows.Forms.ControlStyles.ContainerControl, true);
        }
    }
}