/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScintillaNET
{
    /// <summary>
    /// Specifies how tab characters are drawn when whitespace is visible.
    /// </summary>
    public enum TabDrawMode
    {
        /// <summary>
        /// The default mode of an arrow stretching until the tabstop.
        /// </summary>
        LongArrow = NativeMethods.SCTD_LONGARROW,

        /// <summary>
        /// A horizontal line stretching until the tabstop.
        /// </summary>
        Strikeout = NativeMethods.SCTD_STRIKEOUT
    }
}