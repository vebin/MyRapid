/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Base.Page;
using MyRapid.Code;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyRapid.GameCenter
{
    public partial class Snake : ChildPage
    {
        public Snake()
        {
            InitializeComponent();
        }
        List<GridBlock> body = new List<GridBlock>();
        List<GridBlock> cells = new List<GridBlock>();
        private void Sudoku_Click(object sender, EventArgs e)
        {
            pictureBox1.Focus();
        }

        GridBlock food = new GridBlock();
        private void CreateFood()
        {
            food = cells[DateTime.Now.Millisecond % (cells.Count)];
            cells.Remove(food);
            ShowGrid.PaintBlock(food);
        }


        private FlowDirection SnakeDirection = FlowDirection.BottomUp;
        private void Snake_KeyDown(object sender, KeyEventArgs e)
        {
            
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            //蛇爬行
            //蛇尾抹除足迹
            GridBlock tail = new GridBlock();
            tail.BlockRowIndex = body[body.Count - 1].BlockRowIndex;
            tail.BlockColumnIndex = body[body.Count - 1].BlockColumnIndex;
            tail.BlockColor = Color.Silver;
            for (int i = body.Count - 1; i > 0; i--)
            {
                body[i].BlockRowIndex = body[i - 1].BlockRowIndex;
                body[i].BlockColumnIndex = body[i - 1].BlockColumnIndex;
            }
            GridBlock heard = body[0];
            switch (SnakeDirection)
            {
                case FlowDirection.LeftToRight:
                    heard.BlockColumnIndex += 1;
                    break;
                case FlowDirection.TopDown:
                    heard.BlockRowIndex += 1;
                    break;
                case FlowDirection.RightToLeft:
                    heard.BlockColumnIndex -= 1;
                    break;
                case FlowDirection.BottomUp:
                    heard.BlockRowIndex -= 1;
                    break;
                default:
                    break;
            }
            //出界判定失败
            if (heard.BlockRowIndex < 0 || heard.BlockRowIndex > 27
            || heard.BlockColumnIndex < 0 || heard.BlockColumnIndex > 27)
            {
                DeadDraw();
                timer1.Enabled = false;
            }
            if (body.Where(b => b.BlockRowIndex.Equals(heard.BlockRowIndex)
          && b.BlockColumnIndex.Equals(heard.BlockColumnIndex)).Count() > 1)
            {
                DeadDraw();
                timer1.Enabled = false;
            }
            foreach (GridBlock gb in body)
            {
                ShowGrid.PaintBlock(gb);
            }
            ShowGrid.PaintBlock(tail);
            ShowGrid.PaintBlock(food);
            if (food.BlockRowIndex.Equals(heard.BlockRowIndex)
            && food.BlockColumnIndex.Equals(heard.BlockColumnIndex))
            {
                GridBlock gb = new GridBlock();
                gb.BlockColor = Color.Brown;
                body.Add(gb);
                CreateFood();
            }

        }

        private void DeadDraw()
        {
            foreach (GridBlock gb in body)
            {
                gb.BlockColor = Color.Gray;
                ShowGrid.PaintBlock(gb);
            }
        }

        private void pictureBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    StartGame();
                    break;
                case Keys.Down:
                    if (SnakeDirection.Equals(FlowDirection.TopDown) && timer1.Interval > 50)
                        timer1.Interval -= 50;
                    else
                        timer1.Interval = 200;
                    if (SnakeDirection != FlowDirection.BottomUp)
                        SnakeDirection = FlowDirection.TopDown;
                    break;
                case Keys.Up:
                    if (SnakeDirection.Equals(FlowDirection.BottomUp) && timer1.Interval > 50)
                        timer1.Interval -= 50;
                    else
                        timer1.Interval = 200;
                    if (SnakeDirection != FlowDirection.TopDown)
                        SnakeDirection = FlowDirection.BottomUp;
                    break;
                case Keys.Left:
                    if (SnakeDirection.Equals(FlowDirection.RightToLeft) && timer1.Interval > 50)
                        timer1.Interval -= 50;
                    else
                        timer1.Interval = 200;
                    if (SnakeDirection != FlowDirection.LeftToRight)
                        SnakeDirection = FlowDirection.RightToLeft;
                    break;
                case Keys.Right:
                    if (SnakeDirection.Equals(FlowDirection.LeftToRight) && timer1.Interval > 50)
                        timer1.Interval -= 50;
                    else
                        timer1.Interval = 200;
                    if (SnakeDirection != FlowDirection.RightToLeft)
                        SnakeDirection = FlowDirection.LeftToRight;
                    break;
                default:
                    break;
            }
        }

        private void StartGame ()
        {
            body.Clear();
            cells.Clear();
            ShowGrid.BackgroundColor = Color.White;
            ShowGrid.BlockOffset = 0;
            ShowGrid.BorderColor = Color.Silver;
            ShowGrid.BorderSize = 1;
            //ShowGrid.BlockFont = new Font("微软雅黑", 28);
            ShowGrid.BlockColor = Color.Silver;
            ShowGrid.GridColumnCount = 28;
            ShowGrid.GridRowCount = 28;
            ShowGrid.BlockWidth = (pictureBox1.Width - ShowGrid.BlockOffset) / ShowGrid.GridColumnCount - ShowGrid.BlockOffset;
            ShowGrid.BlockHeight = (pictureBox1.Height - ShowGrid.BlockOffset) / ShowGrid.GridRowCount - ShowGrid.BlockOffset;
            ShowGrid.InitializeGrid(pictureBox1);
            for (int i = 0; i < 28; i++)
            {
                for (int j = 0; j < 28; j++)
                {
                    GridBlock cell = new GridBlock();
                    cell.BlockRowIndex = i;
                    cell.BlockColumnIndex = j;
                    cell.BlockColor = Color.Blue;
                    //cell.BorderColor = Color.Gray;
                    cells.Add(cell);
                }
            }

            GridBlock gb = new GridBlock();
            //gb.BlockIndex = 14 * 28 + 14;
            gb.BlockRowIndex = 20;
            gb.BlockColumnIndex = 14;
            gb.BlockColor = Color.Brown;
            body.Add(gb);
            ShowGrid.PaintBlock(gb);
            gb = new GridBlock();
            //gb.BlockIndex = 14 * 28 + 14;
            gb.BlockRowIndex = 21;
            gb.BlockColumnIndex = 14;
            gb.BlockColor = Color.Brown;
            body.Add(gb);
            ShowGrid.PaintBlock(gb);
            gb = new GridBlock();
            //gb.BlockIndex = 14 * 28 + 14;
            gb.BlockRowIndex = 22;
            gb.BlockColumnIndex = 14;
            gb.BlockColor = Color.Brown;
            body.Add(gb);
            ShowGrid.PaintBlock(gb);
            SnakeDirection = FlowDirection.BottomUp;
            CreateFood();
            timer1.Enabled = true;
        }

 
    }
}