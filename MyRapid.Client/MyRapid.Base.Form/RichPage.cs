/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraEditors;
using MyRapid.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
namespace MyRapid.Base.Page
{
    public partial class RichPage : XtraForm
    {
        public RichPage()
        {

            InitializeComponent();
            CancelButton = new BarItemEx(barCancel, DialogResult.Cancel);
            AcceptButton = new BarItemEx(barOk, DialogResult.OK);
        }
        public string RtfText
        {
            get
            {
                return rtfEdit.RtfText;
            }
            set
            {
                rtfEdit.RtfText = value;
            }
        }
        private void barOk_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void barCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}