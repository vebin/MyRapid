/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Designer;
using MyRapid.Code;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyRapid.Base.Designer
{
    public class ChartControlDesigner
    {
        //存储编辑后的rtf
        public string Content;
        //存储编辑后的Stream
        public MemoryStream StreamContent = new MemoryStream();
        //存储编辑后的Byte[]
        public byte[] ByteContent;
        //存储源文件路径
        private string SourceFile;
        public ChartControlDesigner(string fileName)
        {
            try
            {
                if (string.IsNullOrEmpty(fileName)) return;
                ChartControl chartControl = new ChartControl();
                if (File.Exists(fileName))
                {
                    chartControl.LoadFromFile(fileName);
                    SourceFile = fileName;
                }
                else
                {
                    File.WriteAllText("tmp", fileName);
                    chartControl.LoadFromFile("tmp");
                    SourceFile = "tmp";
                    Content = fileName;
                }
                ChartDesigner chartDesigner = new ChartDesigner(chartControl);
                if (chartDesigner.ShowDialog().Equals(DialogResult.OK))
                {
                    chartControl.SaveToStream(StreamContent);
                    ByteContent = StreamContent.ToArray();
                    chartControl.SaveToFile("tmp");
                    Content = File.ReadAllText("tmp");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ChartControlDesigner(byte[] bytes)
        {
            try
            {
                if (bytes == null) return;
                ChartControl chartControl = new ChartControl();
                chartControl.LoadFromStream(new MemoryStream(bytes));
                ChartDesigner chartDesigner = new ChartDesigner(chartControl);
                if (chartDesigner.ShowDialog().Equals(DialogResult.OK))
                {
                    chartControl.SaveToStream(StreamContent);
                    ByteContent = StreamContent.ToArray();
                    chartControl.SaveToFile("tmp");
                    Content = File.ReadAllText("tmp");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}