/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.IO;
using DevExpress.XtraRichEdit;
using MyRapid.Code;

namespace MyRapid.Base.Designer
{
    public partial class DiagramDesigner : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public DiagramDesigner()
        {
            InitializeComponent();
        }

        //存储编辑后的rtf
        public string Content;
        //存储编辑后的Byte[]
        public byte[] ByteContent;
        //存储源文件路径
        private string SourceFile;
        public DiagramDesigner(string fileName)
        {
            InitializeComponent();
            try
            {
                if (string.IsNullOrEmpty(fileName)) return;
                if (File.Exists(fileName))
                {
                    diagramControl1.LoadDocument(fileName);
                    SourceFile = fileName;
                }
                else
                {
                    File.WriteAllText("tmp", fileName);
                    diagramControl1.LoadDocument(fileName);
                    SourceFile = "tmp";
                }
                diagramControl1.SaveDocument("tmp");
                Content = File.ReadAllText("tmp");
            }
            catch
            {
                throw;
            }
        }

        public DiagramDesigner(byte[] bytes)
        {
            InitializeComponent();
            try
            {
                if (bytes == null) return;
                File.WriteAllBytes("tmp", bytes);
                diagramControl1.LoadDocument("tmp");
                ByteContent = bytes;
            }
            catch
            {
                throw;
            }
        }

        private void DiagramDesigner_FormClosing(object sender, FormClosingEventArgs e)
        {
            diagramControl1.SaveDocument("tmp");
            Content = File.ReadAllText("tmp");
            ByteContent = File.ReadAllBytes("tmp");
        }
    }
}