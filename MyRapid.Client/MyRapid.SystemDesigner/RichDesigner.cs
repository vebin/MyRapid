/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraRichEdit;
using System.IO;
using MyRapid.Code;

namespace MyRapid.Base.Designer
{
    public partial class RichDesigner : RibbonForm
    {
        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
        void InitializeRichEditControl()
        {

        }
        public RichDesigner()
        {
            InitializeComponent();
            InitSkinGallery();
            InitializeRichEditControl();
            ribbonControl.SelectedPage = homeRibbonPage1;
        }

        //存储编辑后的rtf
        public string Content;
        //存储编辑后的Byte[]
        public byte[] ByteContent;
        //存储Stream 的 DocumentFormat
        private DocumentFormat StreamFormat = DocumentFormat.Rtf;
        //存储源文件路径
        private string SourceFile;

        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径 或 Rtf 或 Text</param>
        public RichDesigner(string fileName)
        {
            InitializeComponent();
            InitSkinGallery();
            InitializeRichEditControl();
            ribbonControl.SelectedPage = homeRibbonPage1;
            try
            {
                if (string.IsNullOrEmpty(fileName)) return;
                if (File.Exists(fileName))
                {
                    richEditControl.LoadDocument(fileName);
                    SourceFile = fileName;
                }
                else
                {
                    richEditControl.RtfText = fileName;
                    if (string.IsNullOrEmpty (richEditControl.Text))
                    {
                        richEditControl.Text = fileName;
                    }
                }
                Content = richEditControl.RtfText;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 传入字节
        /// </summary>
        /// <param name="bytes">文件字节</param>
        public RichDesigner(byte[] bytes)
        {
            InitializeComponent();
            InitSkinGallery();
            InitializeRichEditControl();
            ribbonControl.SelectedPage = homeRibbonPage1;
            try
            {
                if (bytes == null) return;
                File.WriteAllBytes("tmp", bytes);
                richEditControl.LoadDocument("tmp", DocumentFormat.Rtf);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void iExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        //保存结果
        private void RichDesigner_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                richEditControl.SaveDocument("tmp",DocumentFormat .Rtf );
                ByteContent = File.ReadAllBytes ("tmp");
                Content = richEditControl.RtfText;
                if (!string.IsNullOrEmpty(SourceFile))
                {
                    richEditControl.SaveDocument();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}