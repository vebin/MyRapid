/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraRichEdit;
using MyRapid.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyRapid.Base.Designer
{
    public partial class SnapDesigner : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public SnapDesigner()
        {
            InitializeComponent();
        }

        //存储编辑后的rtf
        public string Content;
        //存储编辑后的Byte[]
        public byte[] ByteContent;
        //存储源文件路径
        private string SourceFile;

        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径 或 Rtf 或 Text</param>
        public SnapDesigner(string fileName)
        {
            InitializeComponent();
            try
            {
                if (string.IsNullOrEmpty(fileName)) return;
                if (File.Exists(fileName))
                {
                    snapControl1.LoadDocument(fileName);
                    SourceFile = fileName;
                }
                else
                {
                    snapControl1.RtfText = fileName;
                    if (string.IsNullOrEmpty (snapControl1.Text ))
                    {
                        snapControl1.Text = fileName;
                    }
                }
                Content = snapControl1.RtfText;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 传入字节
        /// </summary>
        /// <param name="bytes">文件字节</param>
        public SnapDesigner(byte[] bytes)
        {
            InitializeComponent();
            try
            {
                if (bytes == null) return;
                snapControl1.SnxBytes= bytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SnapDesigner_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                ByteContent = snapControl1.SnxBytes;
                Content = snapControl1.RtfText;
                if (!string.IsNullOrEmpty(SourceFile))
                {
                    snapControl1.SaveDocument();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}