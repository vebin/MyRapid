/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.Utils;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyRapid.Data2
{
    public class IconList2
    {
        public void GetAll()
        {
            MyRapid.Data.IconList il = new Data.IconList();
            Assembly[] assemblyList = AppDomain.CurrentDomain.GetAssemblies();//  .LoadFrom(BaseService.Session.StartupPath + "MyIcon.dll");
            foreach (Assembly assembly in assemblyList )
            {
                AssemblyName aName = assembly.GetName();
                
                Console.WriteLine(aName.Name);
            }
        }
    }

}