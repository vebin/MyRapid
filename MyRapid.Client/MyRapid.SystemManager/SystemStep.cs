/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Base.Page;
using MyRapid.Client.Service;
using MyRapid.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MyRapid.SystemManager
{
    public partial class SystemStep : ChildPage
    {
        public SystemStep()
        {
            InitializeComponent();
        }
        private void gc_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.BaseButtonEventArgs e)
        {
            DataTable sdt = BaseService.Open("SystemStep_Default", null);
            DataTable tdt = (DataTable)gdc.DataSource;
            foreach (DataRow dr in sdt.Rows)
            {
                dr.SetAdded();
            }
            tdt.Merge(sdt);
        }
    }
}