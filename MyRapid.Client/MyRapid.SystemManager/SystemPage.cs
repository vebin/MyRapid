/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraEditors;
using MyRapid.Base.Page;
using MyRapid.Client.Service;
using MyRapid.Client.Service.MainService;
using MyRapid.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MyRapid.SystemManager
{
    public partial class SystemPage : Base.Page.ChildPage
    {
        public SystemPage()
        {
            InitializeComponent();
        }
        private void SystemPage_Load(object sender, EventArgs e)
        {
        }
        public override void Copy()
        {
            try
            {
                if (gv.FocusedRowHandle < 0) return;
                string g = (string)gv.GetFocusedRowCellValue("Page_Id");
                List<MyParameter> mps = new List<MyParameter>();
                mps.Add("@OPAGE_ID", DbType.String, g, null);
                BaseService.Execute("SystemPage_Copy", mps);
            }
            catch (Exception ex)
            {
                SharedFunc.RaiseError(ex);
            }
        }

        private void rPage_Class_MouseDown(object sender, MouseEventArgs e)
        {
            ComboBoxEdit comboBoxEdit = (ComboBoxEdit)sender;
            string pagePath = gv.GetFocusedRowCellValue("Page_Path").ToStringEx();
            if (!string.IsNullOrEmpty(pagePath))
            {
                if (!File.Exists(pagePath))
                {
                    pagePath = SharedFunc.SearchFile(Application.StartupPath, pagePath);
                }
                if (File.Exists(pagePath))
                {
                    comboBoxEdit.Properties.Items.Clear();
                    System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(pagePath);
                    foreach (var item in assembly.GetTypes().OrderBy(a => a.FullName))
                    {
                        if (item.IsPublic)
                            comboBoxEdit.Properties.Items.Add(item.FullName);
                    }
                }
            }
        }
        private void rPage_Class_BeforePopup(object sender, EventArgs e)
        {
        }
    }
}