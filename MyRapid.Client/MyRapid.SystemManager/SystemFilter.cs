/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using MyRapid.Base.Page;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MyRapid.SystemManager
{
    public partial class SystemFilter : ChildPage
    {
        public SystemFilter()
        {
            InitializeComponent();
        }
        private string EditPage;
        public SystemFilter(string pageId, GridControl grid)
        {
            InitializeComponent();
            EditPage = pageId;
            fSql.SourceControl = grid;
        }
        private void fSql_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            string w = DevExpress.Data.Filtering.CriteriaToWhereClauseHelper.GetMsSqlWhere(fSql.FilterCriteria);
            //\"([^\"]*)\"
            fWhere.Text = w.Replace("\"", "");
        }
        private void SystemFilter_Shown(object sender, EventArgs e)
        {
            //默认值
            Sys_Bind bind = BindList.Find(b => b.Bind_Name == "@cFilter_Track");
            if (bind != null)
            {
                BaseEdit baseEdit = (BaseEdit)bind.Bind_Control;
                baseEdit.Text = EditPage;
            }
        }
    }
}