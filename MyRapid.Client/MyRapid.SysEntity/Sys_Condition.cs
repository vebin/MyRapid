/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
namespace MyRapid.SysEntity
{
    ///<summary>
    ///表Sys_Condition的实体类
    ///</summary>
    public class Sys_Condition
    {
        public string Condition_Id { get; set; }
        public string Condition_Name { get; set; }
        public string Condition_Nick { get; set; }
        public int Condition_ForeColor { get; set; }
        public int Condition_BackColor { get; set; }
        public bool Condition_WholeRow { get; set; }
        public string Condition_Expression { get; set; }
        public bool IsDelete { get; set; }
        public string Remark { get; set; }
        public string Create_User { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Modify_User { get; set; }
        public System.DateTime Modify_Time { get; set; }
        public string Delete_User { get; set; }
        public System.DateTime Delete_Time { get; set; }
    }
}